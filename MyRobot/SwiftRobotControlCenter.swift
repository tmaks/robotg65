//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//all robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    
    //in this function change levelName
    override func viewDidLoad() {
        levelName = "L55H" // level name
        
        super.viewDidLoad()
        
    }
    
    //write your code here
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        func turnLeft() {
            turnRight()
            turnRight()
            turnRight()
        }
        
        func Diagonal1() {
            put()
            move()
            turnRight()
            move()
            turnLeft()
        }
        
        func Diagonal2() {
            put()
            move()
            turnRight()
            move()
            turnLeft()
            
        }
        
        Diagonal1()
        while (frontIsClear) {
            if (rightIsClear) {
                Diagonal1()
            } else {
                break
            }
        }
        turnRight()
        turnRight()
        put()
        while (frontIsClear) {
            if (leftIsBlocked) {
                move()
            } else {
                break
            }
        }
        turnRight()
        Diagonal2()
        while (frontIsClear) {
            if (rightIsClear) {
                Diagonal2()
            } else {
                break
            }
        }
        put()
    }
}
