//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//all robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
	
	
	//in this function change levelName
	override func viewDidLoad() {
		levelName = "L22H" // level name
		
		super.viewDidLoad()
		
	}
	
	//write your code here
	override func viewDidAppear(_ animated: Bool) {
		
        super.viewDidAppear(animated)
		
        func turnLeft() {
        turnRight()
        turnRight()
        turnRight()
        }
        
        func nextColumn() {
            put()
            turnRight()
            move()
            move()
            move()
            move()
            turnRight()
            move()
            move()
            move()
            move()
            move()
            move()
            move()
            move()
            turnLeft()
            turnLeft()
        }

        turnLeft()
        
        for _ in 0..<3 {
            while (frontIsClear) {
            if (noCandyPresent) {
                put()
                move()
            } else {
                move()
            }
        }
        nextColumn()
        }
        while (frontIsClear) {
            if (noCandyPresent) {
                put()
                move()
            } else {
                move()
            }
        }
        put()
    }
}
